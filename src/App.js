import './App.css';
import Home from './pages/home/'
import store from './store/store'
import {Provider} from 'react-redux'
import {Route, BrowserRouter} from 'react-router-dom';
import ProductDetails from './pages/productDetail'
import Basket from './pages/basket'
import Login from './pages/login'
import AdminPanel from './pages/admin'
function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <BrowserRouter>
          <Route exact path='/' component={Home}/>
          <Route exact path='/basket' component={Basket}/>
          <Route exact path='/login' component={Login}/>
          <Route exact path='/admin' component={AdminPanel}/>
          <Route exact path='/product/:productId' component={ProductDetails}/>
        </BrowserRouter>
      </div>
    </Provider>
    
  );
}

export default App;
