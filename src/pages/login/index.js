import React, {useEffect,useState} from 'react';
import './index.css';
import Header from '../../components/header'
import Footer from '../../components/footer'
import {withRouter,useHistory} from "react-router-dom"
import {connect} from 'react-redux'
import {getUsers,login} from '../../store/actions/userActions'

function Login(props){
    let history = useHistory();
    useEffect(() => {
        props.getUsers()
    }, []);
    const {users} = props.userReducer
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const checkUser = () => {
        let flag = true
        users?.map(user => {
            if (user.username === username && user.password === password){
                localStorage.setItem('username',username)
                localStorage.setItem('isAuth',true)
                flag = false
                if(username=='admin'){
                    history.push("/admin");
                }else{
                    history.push("/basket");
                }
                return 0
            }
        })   
        if(flag){
            let userData = {
                id: Math.random(),
                username: username,
                password: password
            }
            localStorage.setItem('username',username)
            localStorage.setItem('isAuth',true)
            props.login(userData,history)
        }  
    }
    return(
        <div>
            <Header /> 
            <div className='login'>
                <h1>Login</h1>
                <form>
                    <label>Username or email address *</label>
                    <input value={username} onChange={(e) => setUsername(e.target.value)}/>
                    <label>Password *</label>
                    <input type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                    <button onClick={() => checkUser()}>Login</button>
                    <p>Lost your password?</p>
                </form>
            </div> 
            <Footer />
        </div>
    )
}

const mapStateToProps=(state)=>({
  userReducer: state.userReducer
})

export default connect(mapStateToProps, {login,getUsers})(withRouter(Login))