import React, {useEffect,useState} from 'react';
import Header from '../../components/header'
import Footer from '../../components/footer'
import {Link, withRouter} from 'react-router-dom'
import {getProductDetails} from '../../store/actions/productActions'
import './index.css'
import {addToBasket} from '../../store/actions/basketAction'
import { connect } from 'react-redux';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';


function ProductDetails(props){
    useEffect(() => {
        props.getProductDetails(props.match.params.productId)
    }, []);
   const [showFullImg,setShowFullImg] = useState(false)
   const [showViewCard,setShowViewCard] = useState(false)
   const [amountInput,setAmountInput] = useState(0)

   const {productDetails} = props.productReducer
   const fullImg =  <div className='full-img' onClick={() => setShowFullImg(false)}>
                        <img src={productDetails.image} />
                    </div>
    let addItem = () =>{
        let data = {
            amount: amountInput,
            productID: props.match.params.productId
        }
        props.addToBasket(data,setShowViewCard)
    }
    const viewCard = <div className="viewCard">
        <p>'{productDetails.name}' has been added to your cart.</p>
        <Link to='/basket'><button>View Card</button></Link>
    </div>
    return(
        <div>
            <Header /> 
            <div className='row'>
                { showViewCard ? viewCard : ''}
                <div className='product-item'>
                    <button className='full-btn' onClick={() => setShowFullImg(true)}><img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/search-icon.png' alt='logo'/></button>
                    <img src={productDetails.image} />
                </div>
                <div className='product-item'>
                    <h1>{productDetails.name}</h1>
                    <p className='price-p'>${productDetails.price}</p>
                    <p>{productDetails.description}</p>
                    <div className='add-card'>
                        <input type='number' min="0" max={productDetails.quantity} name="amountInput" onChange={(e) => setAmountInput(e.target.value)}></input>
                        <button onClick={() => addItem()}>Add to card</button>
                    </div>
                    <p>SKU: Relax599 Categories: Accessories, All Product, New Arrivals</p>
                </div>
                <Tabs style={{'marginTop': "30px"}}>
                    <TabList>
                    <Tab>Description</Tab>
                    </TabList>
                    <TabPanel>
                    <p>{productDetails.description}</p>
                    </TabPanel>
                </Tabs>
            </div>
            {showFullImg ? fullImg : ''}
            <Footer />
        </div>
    )
}

const mapStateToProps=(state)=>({
    productReducer: state.productReducer
})

export default connect(mapStateToProps, {getProductDetails,addToBasket})(withRouter(ProductDetails))