import React, {useEffect} from 'react'
import './index.css'
import {uniqBy, prop} from 'ramda'
import Header from '../../components/header'
import Footer from '../../components/footer'
import {getBasket} from '../../store/actions/basketAction'
import {getProducts} from '../../store/actions/productActions'
import BasketItem from '../../components/basketItem/index'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
function Basket(props){
    useEffect(() => {
        props.getBasket()
        props.getProducts()
    }, []);
    const {basket} = props.basketReducer
    const {products} = props.productReducer

    let basketData = []
    basket?.map(item => {
        products?.map(product => {
            if (+item.productID == product.id){
                basketData.push({...product,amount : item.amount,basketId:item.id})
            }
        })
    })
    let filteredArray = []
    let res = Object.fromEntries(basketData.map(item => [item.id, 0]));
    basketData.forEach(item => {res[item.id] += +item.amount})

    basketData.map(item => {
        Object.keys(res).map(data => {
            if(item.id == +data){
                filteredArray.push({...item,uniqAmount:res[data]})
            }
        })
    })

    return(
        <div>
            <Header />
            <div className="row">
                <div className="items">
                    <BasketItem basketData={uniqBy(prop('id'), filteredArray)}/>
                </div>
                <div className="text-block">
                    <h1>Recent Posts</h1>
                    <p>Shopper Woocommerce wordpress theme will help you sell
                        With This Free Shopper Theme we can Sell Product
                        New Product Will Sell If Your Product is Amazing
                        Creative Blog Post To Sell Your Product
                        Hello world!
                    </p>
                    <h1>Woocommerce Plugin</h1>
                    <p>Shopper Woocommerce wordpress theme will help you sell
                        With This Free Shopper Theme we can Sell Product
                        New Product Will Sell If Your Product is Amazing
                        Creative Blog Post To Sell Your Product
                        Hello world!
                    </p>
                </div>
              
            </div>
            <Footer />
        </div>
    )
}

const mapStateToProps=(state)=>({
    productReducer: state.productReducer,
    basketReducer: state.basketReducer
})

export default connect(mapStateToProps, {getBasket,getProducts})(withRouter(Basket))