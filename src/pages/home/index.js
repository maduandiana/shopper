import React, {useEffect} from 'react';
import Header from '../../components/header'
import Card from '../../components/card'
import Footer from '../../components/footer'
import {getProducts} from '../../store/actions/productActions'
import './index.css'
import { connect } from 'react-redux';

function Home(props){
    useEffect(() => {
        props.getProducts()
    }, []);
    const {products} = props.productReducer
    let productsRow = products?.map(item => (
        <Card products={item}/>
    ))
    return(
        <div>
            <Header />
            <div className='row'>
                {productsRow}
            </div>
            <Footer />
        </div>
    )
}

const mapStateToProps=(state)=>({
    productReducer: state.productReducer
})

export default connect(mapStateToProps, {getProducts})(Home)