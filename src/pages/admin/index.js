import React, { useEffect , useState} from'react'
import './index.css'
import AdminCard from '../../components/card/admincard'
import {connect} from 'react-redux'
import {getProducts, addProducts} from '../../store/actions/productActions'
import {getCategories} from '../../store/actions/categoryActions'

function AdminPanel(props){
    useEffect(()=>{
        props.getProducts()
        props.getCategories()
    },[])
    const [showModal, setShowModal] = useState(false)
    const {products} = props.productReducer
    let productsRow = products?.map(item => (
        <AdminCard products={item} key={item.id}/>
    ))
    const {categories} = props.categoriesReducer

    const [title, settitle] = useState('')
    const [description, setdescription] = useState('')
    const [price, setprice] = useState(0)
    const [quantity, setquantity] = useState(0)
    const [image, setimage] = useState('')
    const [discount, setdiscount] = useState(0)
    const [category, setcategory] = useState(1)
    const [date, setdate] = useState([])

    const prepareData = (day) => {
       if(!date.includes(+day)){
            setdate([...date, +day])
       }else{
            setdate(date.filter(item => item != +day))
       } 
    }
   
    const addProducts = () => {
        let data = {
            name: title,
            description: description,
            price: +price,
            quantity: +quantity,
            image: image,
            discount: +discount,
            category, date
        }
        props.addProducts(data)
    }
         
    const addProductModal = <div className='div' key={11}>
        <form className='form'>
            <input type='text' placeholder='Title' value={title} onChange={(e) => settitle(e.target.value)}/>
            <input type='text' placeholder='Description' value={description} onChange={(e) => setdescription(e.target.value)}/>
            <input type='number' placeholder='Price' value={price} onChange={(e) => setprice(e.target.value)}/>
            <input type='number' placeholder='Quantity' value={quantity} onChange={(e) => setquantity(e.target.value)}/>
            <input type='text' placeholder='Image' value={image} onChange={(e) => setimage(e.target.value)}/>
            <input type='number' placeholder='Discount' value={discount} onChange={(e) => setdiscount(e.target.value)}/>
            <select value={+category} onChange={(e) => setcategory(+e.target.value)}>
                {categories?.map(item => (
                    <option value={+item.id} >{item.category}</option>
                ))}
            </select>
            <select className='date-select' onChange={(e) => prepareData(e.target.value)} multiple={true} value={date}>
                    <option value={1}>Пн</option>
                    <option value={2}>Вт</option>
                    <option value={3}>Ср</option>
                    <option value={4}>Чт</option>
                    <option value={5}>Пт</option>
                    <option value={6}>Сб</option>
                    <option value={7}>Вс</option>
            </select>
            <button onClick={()=>addProducts()}>Добавить продукт</button>
        </form>
    </div>
    return (
        <div>
            <button onClick={() => setShowModal(!showModal)}>Add new Product</button>
            { showModal ? addProductModal : ''}
            <div className='row'> 
               {productsRow}    
            </div>
        </div>
    )
}

const mapStateToProps=(state)=>({
    productReducer: state.productReducer,
    categoriesReducer: state.categoriesReducer
})

export default connect(mapStateToProps, {addProducts,getCategories,getProducts})(AdminPanel)