import {GET_BASKET} from '../actions/types'

const inialState = {
    basket: []
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function(state=inialState, action){
    switch(action.type){
        case GET_BASKET:
            return {
                ...state,
                basket: action.payload
            }
        default:
            return state
    }
}