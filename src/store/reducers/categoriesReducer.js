import {GET_CATEGORIES} from '../actions/types'

const inialState = {
    categories: []
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function(state=inialState, action){
    switch(action.type){
        case GET_CATEGORIES:
            return {
                ...state,
                categories: action.payload
            }
        default:
            return state
    }
}