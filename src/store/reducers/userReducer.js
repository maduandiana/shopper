import {GET_USERS} from '../actions/types'

const inialState = {
    users: []
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function(state=inialState, action){
    switch(action.type){
        case GET_USERS:
            return {
                ...state,
                users: action.payload
            }
        default:
            return state
    }
}