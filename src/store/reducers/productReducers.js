import {GET_PRODUCTS, GET_ERRORS,GET_PRODUCT_DETAILS} from '../actions/types'

const inialState = {
    products: [],
    error: {},
    productDetails: {}
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function(state=inialState, action){
    switch(action.type){
        case GET_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }
        case GET_PRODUCT_DETAILS:
            return {
                ...state,
                productDetails: action.payload
            }
        case GET_ERRORS:
            return {
                ...state,
                error: action.payload
            }
        default:
            return state
    }
}