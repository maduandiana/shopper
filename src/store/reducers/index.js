import productReducer from './productReducers'
import categoriesReducer from './categoriesReducer'
import basketReducer from './basketReducer'
import {combineReducers} from 'redux'
import userReducer from './userReducer'
export default combineReducers({
    productReducer,
    categoriesReducer,
    basketReducer,
    userReducer
})