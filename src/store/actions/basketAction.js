import {GET_BASKET,GET_ERRORS,API} from './types'
import axios from 'axios'

export const addToBasket = (data,setShowViewCard) => dispatch => {
    axios.post(API + 'basket',data).then(
        response => {
            setShowViewCard(true)
            getBasket()
        }
    ).catch(err => {
        return dispatch({
            type: GET_ERRORS,
            payload: err.response
        })
    })
}

export const getBasket = () => dispatch => {
    axios.get('http://localhost:3000/basket').then(
        response => {
            return dispatch ({
                type: GET_BASKET,
                payload: response.data
            })
        }
    ).catch(err => {
        return dispatch({
            type: GET_ERRORS,
            payload: err.response
        })
    })
}

export const deleteBasket = (id) => dispatch => {
    axios.delete('http://localhost:3000/basket/'+id).then(
    ).catch(err => {
        return dispatch({
            type: GET_ERRORS,
            payload: err.response
        })
    })
}

export const updateBasket = (id,data) => dispatch => {
    axios.put('http://localhost:3000/basket/'+id,data).then(
    ).catch(err => {
        return dispatch({
            type: GET_ERRORS,
            payload: err.response
        })
    })
}