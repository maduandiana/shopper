export const GET_PRODUCTS = 'GET_PRODUCTS'
export const GET_PRODUCT_DETAILS = 'GET_PRODUCT_DETAILS'
export const GET_CATEGORIES = 'GET_CATEGORIES'
export const GET_ERRORS = 'GET_ERRORS'
export const API = 'http://localhost:3000/'
export const GET_BASKET = 'GET_BASKET'
export const GET_USERS = "GET_USERS"