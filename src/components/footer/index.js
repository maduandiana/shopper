import React from 'react'
import './index.css'
function Footer(){
    return(
        <div className='footer'>
            <div className='row'>
                <div className='column'>
                    <h1>Shopper WooCommerce Theme</h1>
                    <p>Shopper Free WooCommerce eCommerce WordPress Theme perfect for simple online store, with clean and modern grid layout.</p>
                </div>
                <div className='column'>
                    <h1>Woocommerce Plugin</h1>
                    <p>Easily manage your simple, digital and variable products in Woo Commerce with our intuitive UI, and easy store layout. Create your e-commerce store today.</p>
                </div>
                <div className='column'>
                    <h1>Email Subscribe</h1>
                    <div className='subscribe-input'>
                        <input type='email' placeholder='Enter your Email'></input>
                        <button>GO</button>
                    </div>
                </div>
            </div>
            <div className='center'>
                <div className='icons-row'>
                <a href='https://www.facebook.com/'>
                    <img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/facebook-icon.png' alt='facebook'/>
                </a>
                <a href='https://www.facebook.com/'>
                    <img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/twitter-icon.png' alt='facebook'/>
                </a>
                <a href='https://www.facebook.com/'>
                    <img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/linkedin-icon.png' alt='facebook'/>
                </a>
                <a href='https://www.facebook.com/'>
                    <img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/facebook-icon.png' alt='facebook'/>
                </a>
                <a href='https://www.facebook.com/'>
                    <img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/picasa-icon.png' alt='facebook'/>
                </a>
                <a href='https://www.facebook.com/'>
                    <img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/facebook-icon.png' alt='facebook'/>
                </a>
                <a href='https://www.facebook.com/'>
                    <img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/facebook-icon.png' alt='facebook'/>
                </a>
            </div>
                <p>© 2019 All Rights Reserved.</p>
            </div> 
        </div>
    )
}
export default Footer