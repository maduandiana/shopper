import React,{useState} from 'react'
import './index.css'
import {withRouter} from "react-router-dom"
import {connect} from 'react-redux'
import {deleteBasket,getBasket,updateBasket} from '../../store/actions/basketAction'
function Item(props){
    const {basket} = props.basketReducer
    let deleteItems = (id) => {
        basket?.map(item => {
            if(item.productID == id){
                props.deleteBasket(item.id)
                props.getBasket()
            }
        })    
    }
    const [amount,setAmount] = useState(props.data.uniqAmount)
    let handleChange = e =>{
        setAmount(e.target.value)
        let data = {
            id: props.data.basketId,
            amount: e.target.value,
            productID: props.data.id
        }
        props.updateBasket(props.data.basketId,data)
        props.getBasket()
    }
    return(
         <tr>
            <td id="td">
                <button onClick={()=>deleteItems(props.data.id)}>x</button>
                <img src={props.data.image}/>
            </td>
            <td>{props.data.name}</td>
            <td>{props.data.price}$</td>
            <td><input type='number' name="amountInput" min="1" value={amount} onChange={(e) => handleChange(e) }></input></td>
            <td>{props.data.price * amount}$</td>
        </tr>
    )
}
const mapStateToProps=(state)=>({
    basketReducer: state.basketReducer
})

export default connect(mapStateToProps, {deleteBasket,getBasket,updateBasket})(withRouter(Item))