import React from 'react'
import './index.css'
import {withRouter,Link} from "react-router-dom"
import {deleteBasket,getBasket} from '../../store/actions/basketAction'
import {connect} from 'react-redux'
import Item from './item'
function BasketItem(props){
    let basketRows = props.basketData?.map(item => (
       <Item data={item}/>
    ))
    let sumList = [0]
    props.basketData?.map(item => {
        sumList.push(item.price * item.uniqAmount)
    })
    let totalSum = 0
    totalSum = sumList?.reduce((prev,next) => prev+next)
    const isAuth = localStorage.isAuth
    console.log(typeof isAuth)
    return(
        <div className='basket-item'>
            <table>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                </tr>
              {basketRows}
                <tr>
                    <td colSpan="2">
                        <input placeholder='Coupon code'></input>
                        <button>Apply coupon</button>
                    </td>
                    <td colSpan="3"></td>
                    <td><button>Update card</button></td>
                </tr>
            </table>
            <div className="total-block">
                <h1>Cart totals</h1>
                <table>
                    <tr>
                        <th>Subtotal</th>
                        <td>${totalSum}</td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <td>${totalSum + 10}</td>
                    </tr>
                </table>
                <Link to={isAuth == 'true' ? '/checkout' : '/login'}><button className="checkout-btn">Proceed to checkout</button></Link>
            </div>

        </div>
    )
}
const mapStateToProps=(state)=>({
    basketReducer: state.basketReducer
})

export default connect(mapStateToProps, {deleteBasket,getBasket})(withRouter(BasketItem))