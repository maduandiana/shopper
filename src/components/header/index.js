import React, {useState, useEffect} from 'react'
import {getCategories} from '../../store/actions/categoryActions'
import {connect} from 'react-redux'
import {Link, withRouter, useHistory} from "react-router-dom"

import './index.css'
function Header(props){
    const [menu1,setMenu1] = useState(false)
    const [menu2,setMenu2] = useState(false)
    useEffect(() => {
        props.getCategories()
    },[])
    const {categories} = props.categoriesReducer
    const history = useHistory()
    const isAuth = localStorage.isAuth
    const logout = () => {
        localStorage.removeItem('username')
        localStorage.setItem('isAuth',false)
        history.push('/')
    }
    const searchInput = <div className='search-input'>
                            <input placeholder='Search'/>
                            <img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/search-icon.png' alt='logo'/>
                        </div>
    const submenu1 =  <div className='submenu'>
                        <ul>
                            {categories?.map(item => (
                                <li key={item.id}>{item.category}</li>
                            ))}
                        </ul>
                    </div>
    const submenu2 = <div className='submenu'>
                <ul>
                    <li>Checkout</li>
                    <li>Card</li>
                    {isAuth == 'true' ? 
                    <div>
                        <li><Link to="/profile">My account</Link></li>
                        <li onClick={() => logout()}>Log out</li>
                    </div>:
                    <li><Link to="login">Log in</Link></li>
                    }
                </ul>
            </div>
    return(
        <div className='header'>
            <Link to='/'><img src='http://dessign.net/shopper-woocommerce-theme/wp-content/uploads/2015/03/logo1.png' alt='search'/></Link>
            <nav>
                <ul>
                    <li>Home</li>
                    <li>About</li>
                    <li onClick={() => {setMenu1(!menu1);setMenu2(false)}}>Shop{menu1 ? submenu1 : ''}</li>
                    <li onClick={() => {setMenu2(!menu2);setMenu1(false)}}>My account{menu2 ? submenu2 : ''}</li>
                    <li><Link to='/basket'>Card</Link></li>
                    <li>Blog</li>
                    <li>Contact</li>
                </ul>
            </nav>
            {searchInput}
        </div>
    )
}

const mapStateToProps=(state)=>({
    categoriesReducer: state.categoriesReducer
})

export default connect(mapStateToProps, {getCategories})(withRouter(Header))