import React from 'react'
import './index.css'
import {Link, withRouter} from "react-router-dom"
import {deleteProducts, getProducts} from '../../store/actions/productActions'
import {connect} from 'react-redux'
function Card(props){
    let day = new Date().getDay()

    let showSale = props.products.discountDate?.find(item => item == day)
    const deleteProduct = (id) =>{
        props.deleteProducts(id)
        props.getProducts()
    }
    return(
        <div className='card-item' id="admin-card">
            <Link to={'/product/'+props.products.id}><img src={props.products.image} alt='card'/></Link>
            <div>
                <Link to={'/product/'+props.products.id}><p>{props.products.name}</p></Link>
                <p><span className={showSale ? 'line' : ''}>Цена: ${props.products.price}</span>
                {
                    showSale ?  <span className="price">${ (props.products.price * ( 100 - props.products.discount))/100 }</span> : ''
                }
                </p>
                <p>Количество: {props.products.quantity}</p>
            </div>
           
            <button onClick={()=>(deleteProduct(props.products.id))}>Удалить</button>
        </div>
    )
}

const mapStateToProps=(state)=>({
    basketReducer: state.basketReducer
})

export default connect(mapStateToProps, {deleteProducts,getProducts})(withRouter(Card))