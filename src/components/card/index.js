import React from 'react'
import './index.css'
import {Link, withRouter} from "react-router-dom"


function AdminCard(props){
    let day = new Date().getDay()
    console.log(day)
    let showSale = props.products.date?.find(item => item == day)
    return(
        <div className='card-item'>
            <Link to={'/product/'+props.products.id}><img src={props.products.image} alt='card'/></Link>
            <Link to={'/product/'+props.products.id}><p>{props.products.name}</p></Link>
            <p><span className={showSale ? 'line' : ''}>${props.products.price}</span>
            {
                showSale ?  <span className="price">${ (props.products.price * ( 100 - props.products.discount))/100 }</span> : ''
            }
            </p>
        </div>
    )
}
export default withRouter(AdminCard)